# consumption

Part of the too_much suite.

Eventually part of a full plan to track your daily general intake.

```plantuml
map Types {
    Type =>
}
map Amounts {
    Amount =>
}
map Units {
    Unit =>
}
map Percentages {
    Percentage =>
}
map Consumptions {
    DateTime =>
    Number =>
    Merk =>
    Type *-> Types
    Amount *--> Amounts
    Unit *---> Units
    Percentage *----> Percentages
}
```