from django.contrib import admin

from .models import (
    TypeOf,
    Amount,
    Unit,
    Percentage,
    Consumption,
)

admin.site.register(
    (
        TypeOf,
        Amount,
        Unit,
        Percentage,
        Consumption,
    )
)
