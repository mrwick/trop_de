from django.db import models


class TypeOf(models.Model):
    """salt, alcohol, water, protein, bière, tofu """
    typeof_name = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.typeof_name}"


class Amount(models.Model):
    """25, 33, 50 """
    amount_name = models.IntegerField()

    def __str__(self):
        return f"{self.amount_name}"


class Unit(models.Model):
    """ml, cl, mg """
    unit_name = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.unit_name}"


class Percentage(models.Model):
    """ 0.00 - 25.00 but fill it in as you go """
    percentage = models.DecimalField(max_digits=3, decimal_places=1)

    def __str__(self):
        return f"{self.percentage}"


class Consumption(models.Model):
    """ the consumption """
    consumption_date = models.DateTimeField("date of consumption")
    number_of = models.IntegerField()
    amount = models.ForeignKey(
        Amount,
        on_delete=models.DO_NOTHING,
    )
    unit = models.ForeignKey(
        Unit,
        on_delete=models.DO_NOTHING,
    )
    type_of = models.ForeignKey(
        TypeOf,
        on_delete=models.DO_NOTHING,
    )
    percentage = models.ForeignKey(
        Percentage,
        on_delete=models.DO_NOTHING,
    )
    nom = models.CharField(max_length=500)

    def __str__(self):
        return f"{self.nom}"
