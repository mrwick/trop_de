from django.urls import path

from . import views


app_name = "consumption"
urlpatterns = [
    path("", views.index, name="index"),
    path("<int:consumption_id>", views.consumption, name="consumption"),
]
