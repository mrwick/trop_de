from django.shortcuts import (
    render,
    get_object_or_404,
)

from .models import Consumption


def index(request):
    consumptions = Consumption.objects.order_by("-consumption_date")
    context = dict(consumptions=consumptions)
    return render(request, "consumption/index.html", context)


def consumption(request, consumption_id):
    consumption = get_object_or_404(Consumption, pk=consumption_id)
    context = dict(consumption=consumption)
    return render(request, "consumption/detail.html", context)
